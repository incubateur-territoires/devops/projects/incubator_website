terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
    helm = {
      source = "hashicorp/helm"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.13.1"
    }
  }
  required_version = ">= 0.14"
}
