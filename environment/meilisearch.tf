module "meilisearch" {
  count   = var.meilisearch_ingress_host == null ? 0 : 1
  source  = "gitlab.com/vigigloo/tools-k8s/meilisearch"
  version = "0.1.0"

  chart_name    = "meilisearch"
  chart_version = "0.1.25"
  image_tag     = "v0.26.1"
  namespace     = module.namespace.namespace
  values = [
    <<-EOT
    ingress:
      enabled: true
      className: null
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: haproxy
      hosts:
        - ${var.meilisearch_ingress_host}
      tls:
        - hosts:
            - ${var.meilisearch_ingress_host}
          secretName: meilisearch-tls
    EOT
  ]
  persistence     = true
  requests_cpu    = "10m"
  requests_memory = null
  limits_cpu      = null
  limits_memory   = "40Mi"
}
