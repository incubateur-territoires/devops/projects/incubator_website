variable "base-domain" {
  type = string
}

variable "project_slug" {
  type = string
}

variable "namespace" {
  type = string
}

variable "project_name" {
  type = string
}

variable "gitlab_environment_scope" {
  type = string
}

variable "namespace_quota_max_cpu" {
  type    = number
  default = 2
}

variable "namespace_quota_max_memory" {
  type    = string
  default = "12Gi"
}

variable "meilisearch_ingress_host" {
  type    = string
  default = null
}

variable "repositories" {
  type    = list(string)
  default = []
}

variable "monitoring_org_id" {
  type = string
}
