locals {
  repositories = [data.gitlab_project.website.id]
}

module "reviews" {
  source                   = "./environment"
  gitlab_environment_scope = "*"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base-domain              = var.dev_base-domain
  namespace                = "${var.project_slug}-reviews"
  repositories             = local.repositories
  project_slug             = var.project_slug
  project_name             = var.project_name

  namespace_quota_max_memory = "16Gi"
  namespace_quota_max_cpu    = 3

  monitoring_org_id = random_string.development_secret_org_id.result
}

module "development" {
  source                   = "./environment"
  gitlab_environment_scope = "development"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base-domain              = var.dev_base-domain
  namespace                = "${var.project_slug}-development"
  repositories             = local.repositories
  project_slug             = var.project_slug
  project_name             = var.project_name

  meilisearch_ingress_host = "search.${var.dev_base-domain}"

  namespace_quota_max_memory = "16Gi"
  namespace_quota_max_cpu    = 3

  monitoring_org_id = random_string.development_secret_org_id.result
}

module "production" {
  source                   = "./environment"
  gitlab_environment_scope = "production"
  kubeconfig               = data.scaleway_k8s_cluster.prod.kubeconfig[0]
  base-domain              = var.prod_base-domain
  namespace                = var.project_slug
  repositories             = local.repositories
  project_slug             = var.project_slug
  project_name             = var.project_name

  meilisearch_ingress_host = "search.${var.prod_base-domain}"

  namespace_quota_max_memory = "16Gi"
  namespace_quota_max_cpu    = 3

  monitoring_org_id = random_string.production_secret_org_id.result
}
