# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/gitlabhq/gitlab" {
  version     = "3.12.0"
  constraints = "~> 3.12.0"
  hashes = [
    "h1:zZKpUsf1STvLLyH30a6ZcjQoARU8MJGbHsGCjYHrnOA=",
    "zh:1c3e89cf19118fc07d7b04257251fc9897e722c16e0a0df7b07fcd261f8c12e7",
    "zh:207f7ffaaf16f3d0db9eec847867871f4a9eb9bb9109d9ab9c9baffade1a4688",
    "zh:2360bdd3253a15fbbb6581759aa9499235de670d13f02272cbe408cb425dc625",
    "zh:2a9bec466163baeb70474fab7b43aababdf28b240f2f4917299dfe1a952fd983",
    "zh:3a6ea735b5324c17aa6776bff3b2c1c2aeb2b6c20b9c944075c8244367eb7e4c",
    "zh:3aa73c622187c06c417e1cd98cd83f9a2982c478e4f9f6cd76fbfaec3f6b36e8",
    "zh:51ace107c8ba3f2bb7f7d246db6a0428ef94aafceca38df5908167f336963ec8",
    "zh:53a35a827596178c2a12cf33d1f0d0b4cf38cd32d2cdfe2443bdd6ebb06561be",
    "zh:5bece68a724bffd2e66293429319601d81ac3186bf93337c797511672f80efd0",
    "zh:60f21e8737be59933a923c085dcb7674fcd44d8a5f2e4738edc70ae726666204",
    "zh:9fb277f13dd8f81dee7f93230e7d1593aca49c4788360c3f2d4e42b8f6bb1a8f",
    "zh:ac63a9b4a3a50a3164ee26f1e64cc6878fcdb414f50106abe3dbeb7532edc8cd",
    "zh:ed083de9fce2753e3dfeaa1356508ecb0f218fbace8a8ef7b56f3f324fff593b",
    "zh:ed966026c76a334b6bd6d621c06c5db4a6409468e4dd99d9ad099d3a9de22768",
  ]
}

provider "registry.opentofu.org/grafana/grafana" {
  version     = "1.28.2"
  constraints = "~> 1.28.2"
  hashes = [
    "h1:iWmL/sFw6oTJFLlhS6l0/CTj8MtmXu3TNYDuxpE0TiE=",
    "zh:037aa0ce2ac27f09a9727a6e74fad08a1592386ffb6099a775340aa8b6fe08bf",
    "zh:1554908741b0cd9513060441c15b1780e9533cd980743f19d857fd36c3909e64",
    "zh:24c427f9153d7d2105f9ccab588f00033af8c4bd71ce269fcbb968eeb3a664e1",
    "zh:2b418f3411345c04a9e1dcb68653ae6b82a4595bc0fdc3760ef8da65dea89443",
    "zh:3070a84eda2fb9f8a9b8047c47dcc82a93aff2e46031a4b3381f98bec80cdc8c",
    "zh:3d5ed4fe4597c3956611b283f0e3a827d2ee372f5f16feeb307fbea9145fc244",
    "zh:4605817458d001b9a4d5e3ee3afa773dbb0f02e8ba9377344daa561ec904dc6c",
    "zh:48c42667d0198dd6e3b4913d276f587c06ffcc75cbe7c2a8bbc379b9af8c1153",
    "zh:6b834b137debbfef6ffdbd2be5d20db5a89b1e79c0f83dac745523f320da0d1c",
    "zh:82bc5302f847ab3294708e55d80267a9ae044855c64475b3f6be1040a6340393",
    "zh:9bf3ecd23ca6d7239b64eafcbc7eec6ad7fb90b723b27d583bb371204d65b0dd",
    "zh:a7a901a195f04d6fc96129be1d75213aad340e1e0276add5ae786b940e95eaa3",
    "zh:aa05281fed6ebfd42d849abe9a0a038b22df2edb54b213d51209081d6a099863",
    "zh:e1df1caa32d10f0a16765ca09a358238deae442f117cd049bf88af96797f3641",
  ]
}

provider "registry.opentofu.org/hashicorp/helm" {
  version = "2.14.0"
  hashes = [
    "h1:ibK3MM61pVjBwBcrro56OLTHwUhhNglvGG9CloLvliI=",
    "zh:1c84ca8c274564c46497e89055139c7af64c9e1a8dd4f1cd4c68503ac1322fb8",
    "zh:211a763173934d30c2e49c0cc828b1e34a528b0fdec8bf48d2bb3afadd4f9095",
    "zh:3dca0b703a2f82d3e283a9e9ca6259a3b9897b217201f3cddf430009a1ca00c9",
    "zh:40c5cfd48dcef54e87129e19d31c006c2e3309ee6c09d566139eaf315a59a369",
    "zh:6f23c00ca1e2663e2a208a7491aa6dbe2604f00e0af7e23ef9323206e8f2fc81",
    "zh:77f8cfc4888600e0d12da137bbdb836de160db168dde7af26c2e44cf00cbf057",
    "zh:97b99c945eafa9bafc57c3f628d496356ea30312c3df8dfac499e0f3ff6bf0c9",
    "zh:a01cfc53e50d5f722dc2aabd26097a8e4d966d343ffd471034968c2dc7a8819d",
    "zh:b69c51e921fe8c91e38f4a82118d0b6b0f47f6c71a76f506fde3642ecbf39911",
    "zh:fb8bfc7b8106bef58cc5628c024103f0dd5276d573fe67ac16f343a2b38ecee8",
  ]
}

provider "registry.opentofu.org/hashicorp/kubernetes" {
  version     = "2.13.1"
  constraints = ">= 2.13.0, ~> 2.13.1"
  hashes = [
    "h1:fY2gWOd+w5CKV+B+T9LugeprJxHjjkaW81QZ4Urpzlk=",
    "zh:15d61e1ea6e428437e5947330a8328ed62b61bca01e0f2584e4e80d0aaea93ab",
    "zh:61183fddab8988a892f459abb38d9d6e50b8db0e2b252e398a10270e9a4f3328",
    "zh:67db11bf1596595ada2ae4e8fce13ac3e7ffd242ff8f6a2440bd8086c0d2cd09",
    "zh:6be96982825d821df7b29dc0c40c2f5807561e6af33e858668d1e06e1df3efc3",
    "zh:6e391b9601349ded72135e5f9159161f3cf9042e272611c2b5d863bda2724302",
    "zh:73dd61f4dc6f03f58c643a5dcee8170520f38ec06248186b651ea3d951ed7d6a",
    "zh:92b2f3f0657cfe0ad7cfba72e89246f5d02be53389a56fc205716b549e4848cd",
    "zh:a7ff7812eebdcaf75bb8937acc742d8f7d796d2df93d2c2feb27c5813e548da1",
    "zh:ac2edd95b356ac0fb48569c825cb91dfc0820c688ef12cc657c2305b8f12be94",
    "zh:eabae340b652d63f8f127771e70af854671bf76014f8c53e1f95b09d42729e0d",
  ]
}

provider "registry.opentofu.org/hashicorp/random" {
  version     = "3.4.3"
  constraints = "~> 3.4.3"
  hashes = [
    "h1:57xIMCTAE78wv9naPFb3atFFEFn3rW1hOFYTrXMk/C0=",
    "zh:40f2ab718f177b0f8ec29da906104583047531de32cd7dc7f005a606a099d474",
    "zh:6a6684084bd1624b93a262663c5849f9efb597c99d6b03eeb6dbd685760561fb",
    "zh:8dc1973537166b468c08526ef38fa353f389df4ae9639cf8591dbc6e6048336b",
    "zh:aa260ea7793988e7f45ea3916ed6e177f8827e9dd3959fe799cbeda2329e7d23",
    "zh:b59bbf92b7be796c1921acf22b40fbb5e699bd3e9bdc06fa27a7e273509e1028",
    "zh:c4603614ca8c73f3c9b00cb4e89d3b859a62864cf09faba2ae376689a354f326",
    "zh:d82ba8432161763525dc7e8f32ac2377ea444829f805511cb90369b147c62b0c",
    "zh:ee058d8839b35e1dbcfea224652e6e921e015d3454e0c06e8afce3516bb50910",
    "zh:efffc2adfbfdbfde4f7fc9f423338c5969054de064b7afcd391fa2c419da2bc2",
    "zh:f0530bdae9985a3be630679d6c29396721616148a08594bb0a55551a69c53c13",
  ]
}

provider "registry.opentofu.org/scaleway/scaleway" {
  version     = "2.2.9"
  constraints = "~> 2.2.0"
  hashes = [
    "h1:yRjYMvMcU9vvHg0zMWhebs6/JOhfQk2tJR2+njthRZI=",
    "zh:0c4474b3a9ab58db565e9df684364088ce9a46de0c6fa3cb610276a6e4bee07d",
    "zh:0de61684c1599e0595c3f252771efbb05c3e3fe12fb67f7e8906a5554cef12d1",
    "zh:13718f2b75d3ec3e4eae19f576bb01676242c23c14d2e9f7707ee6ab7f2bc636",
    "zh:1cc3febe7780d3b354de910991591f1ac20a1fe528fcbc23703c7e406a6e7c17",
    "zh:2fc2f95aeeec73e4dfc5c703f91a6aee7264d68edde99d5fd6f8881ccc44ff8f",
    "zh:38dbd61499e653092b08e353abda6d2fe0362685cff96ab4127ec556c6c694b6",
    "zh:3a57afe1f8142ed8995808dcee2f1f9373267a2fc58ade7730962cd338057b89",
    "zh:5410ae3e5ffbdd480d39af1fa6154783c8b13026cd8ff90d39b54f1aafaf3dcb",
    "zh:95a4e2278f49a35ac5b345d2ffdcf7f624fe3b5922b03c23ab1e8432690708cf",
    "zh:ba7acf4715085135c384d93f0ca74933e02d1beda68ce4c3cf2cbb3241cf3625",
    "zh:cfa4334f747651c7641790b7e2da913a38cfb7f68122b37c684c421a65be8528",
    "zh:d99969b25c6e65658e78bc26e48debd9e4cb66b5419ec551b41bd8ada92acf4b",
    "zh:defbe054accb2abaeb8a8b9116e7fab63b82b91282a89fa2cf279469b0910f2a",
    "zh:f0e76c46a6d34a7f6b471880445c7f48c52ccd9a3cf83563cb52d5bdde3cd3dc",
  ]
}
