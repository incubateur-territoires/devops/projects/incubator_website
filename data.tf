data "scaleway_k8s_cluster" "prod" {
  provider   = scaleway.scaleway_production
  cluster_id = var.scaleway_cluster_production_cluster_id
}

data "scaleway_k8s_cluster" "dev" {
  provider   = scaleway.scaleway_development
  cluster_id = var.scaleway_cluster_development_cluster_id
}

data "gitlab_project" "website" {
  id = 33006443
}
